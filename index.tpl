<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Revischool</title>
    <link rel="stylesheet" href="static/css/styles.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/js/all.min.js"></script>
</head>
<body>
    <section class="top-page">
        <header class="header">
            <img src="Logo Revischool.jpg" alt="Logo du site">
            <nav class="nav">
                <li class="btn"> <a href="/"> Acceuil </a></li>
                <li class="btn"> <a href="/fiches_de_révisions"> Fiche de revisions </a></li>
                <li class="btn"> <a href="/aide"> Aide </a></li>
				<li class="btn"> <a href="/PageFormulaire">--> Inscription </a></li>
				<li class="btn"> <a href="/liste">--> voir la liste des utilisateurs</a></li>
            </nav>
        </header>
        <div class="landing-page">
            <h1><center>Le pouvoir c'est le savoir</center></h1>
            <a class="scroll-down" href="#services">Scroll <i class="fas fa-angle-down"></i></a>
        </div>
        
    </section>
