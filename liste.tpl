<!doctype html>
<html  lang="fr-FR">

	<head>
		<meta charset="utf-8">
	</head>
	<h3>Liste des utilisateurs</h3>
	<body>

		% titres = ["id", "prenom", "nom", "Niveau_de_classe", "Date_de_naissance", "Email"]

		<table border = 1>
			<thead>
		% for titre in titres:
				<td>{{titre}}</td>
		% end
			</thead>
		% for ligne in lignes:
			<tr>
			% for col in ligne:
				<td>{{col}}</td>
			% end
			</tr>
		% end
		</table>

		<h2><a href = "/">Retourner à la page d'accueil</a></h2>
		
	</body>

</html>
