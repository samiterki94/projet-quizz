import bottle
import sqlite3
import os

my_module = os.path.abspath(__file__)
parent_dir = os.path.dirname(my_module)
static_dir = os.path.join(parent_dir, 'static')
    
conn=sqlite3.connect('base_de_donnees_site.db')
db=conn.cursor()

@bottle.route('/') #on récupère la page index.tpl
def index():
    return bottle.template("index.tpl")

@bottle.route('/PageFormulaire') #on récupère la page formulaire.tpl
def AfficherFormulaire():
    return bottle.template("formulaire.tpl")
    
@bottle.route('/ajouteEntree', method='POST') #on récupère les données saisies dans le formulaire dans une base de données
def ajouteEntree():
    prenom = bottle.request.forms['prenom']
    nom = bottle.request.forms['nom']
    niveau= bottle.request.forms['niveau']
    date_naiss = bottle.request.forms['date']
    mail = bottle.request.forms['mail']
    db.execute("INSERT INTO Utilisateur (prenom,nom,Niveau_de_classe,Date_de_naissance,Email) VALUES (?,?,?,?,?)",(prenom,nom,niveau,date_naiss,mail))
    conn.commit()
    return bottle.template("index.tpl")

@bottle.route('/liste') #on affiche la liste des utilisateurs
def liste():
    req = db.execute("SELECT * FROM Utilisateur")
    lignes = req.fetchall()
    return bottle.template("liste.tpl", lignes=lignes)

@bottle.route('/fiches_de_révisions') #on affiche la liste des fiches de révisions
def liste_cours():
    req = db.execute("SELECT * FROM Cours")
    lignes = req.fetchall()
    return bottle.template("fiches_de_révisions.tpl", lignes=lignes)

@bottle.route('/aide')
def aide():
    return bottle.template("aide.tpl")

bottle.run(bottle.app(), host='0.0.0.0', port=7000, debug= True, reloader=True)
